// Ken Vicchiollo
// Lab Exercise #2 - Playing Cards
// September 8, 2020

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	Club, 
	Diamond,
	Heart,
	Spade
};

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{






	(void)_getch();
	return 0;
}